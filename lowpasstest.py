import numpy as np
from scipy.signal import butter, lfilter, freqz
import matplotlib.pyplot as plt
import scipy


def butter_lowpass(cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    return b, a

def butter_lowpass_filter(data, cutoff, fs, order=5):
    b, a = butter_lowpass(cutoff, fs, order=order)
    y = lfilter(b, a, data)
    return y


# Filter requirements.
order = 6
fs = 30.0       # sample rate, Hz
cutoff = 3.667  # desired cutoff frequency of the filter, Hz

# Get the filter coefficients so we can check its frequency response.
b, a = butter_lowpass(cutoff, fs, order)

# Plot the frequency response.
w, h = freqz(b, a, worN=8000)
time = np.fft.ifft(abs(h))

rolled = np.roll(time, len(time)/2)

trunc = rolled[len(rolled)/2-32:len(rolled)/2+32]

res = np.zeros(len(trunc))
hamming = scipy.signal.hamming(len(trunc))
for i in range(0, len(res)-1):
    res[i] = trunc[i] * hamming[i]

padded = np.lib.pad(res, (0, 1024-len(res)), 'constant', constant_values=0)

ffted = np.fft.fft(padded)

plt.plot(abs(ffted))
plt.show()